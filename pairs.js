function pairs(object){

    if (object === undefined || typeof object !== 'object')
        return [];

    else{

        let keyValuePair = [];

        for (let key in object){
            keyValuePair.push([key, object[key]]);
        }

        return keyValuePair;

    }

}

module.exports = pairs;