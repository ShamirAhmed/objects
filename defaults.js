function defaults(object, defaultProps){

    if (object === undefined || typeof object !== 'object' || defaultProps === undefined || typeof defaultProps !== 'object')
        return [];

    else{
        console.log("yes");
        for (let key in defaultProps){
            if(key in object === false)
                object[key] = defaultProps[key];
        }

        return object;
    }

}

module.exports = defaults;