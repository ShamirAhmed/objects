function keys(object){

    if (object === undefined || typeof object !== 'object'){
        return [];
    }

    else{
        let names = [];

        for(let keyName in object){
            names.push(keyName);
        }

        return names;
    }


}


module.exports = keys;