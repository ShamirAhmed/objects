function values(object){

    if (object === undefined || typeof object !== 'object')
        return [];

    else{
        let valuesInObject = [];

        for (let keyName in object){

            if (typeof object[keyName] !== 'function')
                valuesInObject.push(object[keyName]);
        
        }

        return valuesInObject;
    }

}

module.exports = values;