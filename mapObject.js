function mapObject(object, callback){

    if(object === undefined || callback === undefined || typeof callback !== 'function')
        return [];

    else{

        let newObject = {};

        for(let key in object){
            newObject[key] = callback(object[key], key);
        }

        return newObject;

    }

}

module.exports = mapObject;