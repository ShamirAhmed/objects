function  invert(object){

    if (object === undefined || typeof object !== 'object')
        return [];

    else{

        let invertedObject = {};

        for(let key in object){
            if(typeof object[key] !== 'function')
                invertedObject[object[key]] = key;
        }

        return invertedObject;
    }

}

module.exports = invert;